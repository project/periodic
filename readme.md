Periodic
========

Utility for throttling execution of tasks.

Scheduled Tasks
---------------

The module emits events for hourly, daily, weekly, and monthly intervals that
can be subscribed to.

```
MyModuleEventSubscriber {

  public static function getSubscribedEvents() {
    $events = [];

    $events[PeriodicEvents::HOUR] = ['hourlyTask'];
    $events[PeriodicEvents::DAY] = ['dailyTask'];

    return $events;
  }

  public function hourlyTask() {
    // Do hourly task.
  }

  public function dailyTask() {
    // Do daily task.
  }

}
```

_Note: These events are triggered on cron run, so will execute on the first
cron run following the interval's expiry._

Custom Tasks
------------

Custom tasks can be throttled by arbitrary intervals.

```
function mymodule_cron() {
  $periodManager = \Drupal::service('periodic.manager');

  // Limit task to every six hours.
  if ($periodManager->execute('mymodule.mytask', 21600)) {
    // Do custom task.
  }
}
```
