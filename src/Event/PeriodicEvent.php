<?php

namespace Drupal\periodic\Event;

use Drupal\Component\EventDispatcher\Event;

/**
 * Event for periodic tasks.
 */
class PeriodicEvent extends Event {

  public const INTERVAL_HOUR = 3600;
  public const INTERVAL_DAY = 86400;
  public const INTERVAL_WEEK = 604800;
  // 30 days.
  public const INTERVAL_MONTH = 2592000;

}
