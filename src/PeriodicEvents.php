<?php

namespace Drupal\periodic;

/**
 * Periodic Events constants.
 */
final class PeriodicEvents {
  public const HOUR = 'periodic.hour';
  public const DAY = 'periodic.day';
  public const WEEK = 'periodic.week';
  public const MONTH = 'periodic.month';

}
