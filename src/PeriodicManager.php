<?php

namespace Drupal\periodic;

use Drupal\Component\Datetime\Time;
use Drupal\Core\DestructableInterface;
use Drupal\Core\State\StateInterface;

/**
 * Service for throttling tasks.
 */
class PeriodicManager implements DestructableInterface {

  private const STATE_KEY = 'periodic.tasks';

  /**
   * Task timestamps as loaded from state.
   *
   * @var int[]
   */
  private $originalTasks;

  /**
   * Task timestamps.
   *
   * @var int[]|null
   */
  private $tasks = NULL;

  /**
   * The State service.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * The Time service.
   *
   * @var \Drupal\Component\Datetime\Time
   */
  protected $time;

  /**
   * Initialize Period Manager service.
   *
   * @param \Drupal\Core\State\StateInterface $state
   *   The State service.
   * @param \Drupal\Component\Datetime\Time $time
   *   The Time service.
   */
  public function __construct(StateInterface $state, Time $time) {
    $this->state = $state;
    $this->time = $time;
  }

  /**
   * {@inheritdoc}
   */
  public function destruct() {
    $this->store();
  }

  /**
   * Load data from state to in-memory.
   */
  private function load(): void {
    if (is_null($this->tasks)) {
      $this->originalTasks = $this->tasks = $this->state->get(self::STATE_KEY, []);
    }
  }

  /**
   * Save in-memory data to state storage.
   */
  private function store() {
    // Nothing was loaded or changed during the request.
    if (is_null($this->tasks) || $this->originalTasks == $this->tasks) {
      return;
    }

    if (empty($this->tasks)) {
      // Delete state data if all keys were removed.
      $this->state->delete(self::STATE_KEY);
    }
    else {
      $this->state->set(self::STATE_KEY, $this->tasks);
    }
  }

  /**
   * Get a task timestamp.
   *
   * @param string $key
   *   The task key.
   *
   * @return int|null
   *   The task timestamp, or NULL if not set.
   */
  private function doGet(string $key) {
    $this->load();

    return $this->tasks[$key] ?? NULL;
  }

  /**
   * Set a task timestamp.
   *
   * @param string $key
   *   The task key.
   * @param int|null $value
   *   The timestamp to set, or NULL to set to current time.
   */
  private function doSet(string $key, ?int $value = NULL): void {
    $this->load();

    if (is_null($value)) {
      $value = $this->time->getRequestTime();
    }

    $this->tasks[$key] = $value;
  }

  /**
   * Check if the interval for a task has elapsed.
   *
   * @param string $key
   *   The task key.
   * @param int $interval
   *   The interval to delay the task.
   *
   * @return bool
   *   TRUE if the task's interval has elapsed.
   */
  public function ready(string $key, int $interval): bool {
    $timestamp = $this->doGet($key);

    if (is_null($timestamp)) {
      return TRUE;
    }
    elseif (($timestamp + $interval) < $this->time->getRequestTime()) {
      return TRUE;
    }

    return FALSE;
  }

  /**
   * Update a task's status if its interval has elapsed.
   *
   * @param string $key
   *   The task key.
   * @param int $interval
   *   The interval to delay the task.
   * @param bool $first
   *   Should new tasks execute immediately.
   *
   * @return bool
   *   TRUE if the task's interval has elapsed, and its period has been reset.
   */
  public function execute(string $key, int $interval, bool $first = TRUE): bool {
    $timestamp = $this->doGet($key);

    if (!$first && is_null($timestamp)) {
      $this->doSet($key);
      return FALSE;
    }
    elseif ($this->ready($key, $interval)) {
      $this->doSet($key);
      return TRUE;
    }

    return FALSE;
  }

  /**
   * Reset the interval for a task.
   *
   * A reset task will be ready for execution immediately.
   *
   * @param string $key
   *   The task key.
   */
  public function reset(string $key): void {
    $this->doSet($key, 0);
  }

  /**
   * Remove a task's data.
   *
   * @param string $key
   *   The task key.
   */
  public function remove(string $key): void {
    $this->load();
    unset($this->tasks[$key]);
  }

  /**
   * Remove all task data.
   */
  public function removeAll(): void {
    $this->load();
    $this->tasks = [];
    // self::persist() will delete state if necessary at end of request.
  }

}
