<?php

namespace Drupal\Tests\periodic\Functional;

use Drupal\periodic\PeriodicEvents;
use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\Traits\Core\CronRunTrait;

/**
 * Functional test of Periodic cron execution.
 *
 * @group periodic
 */
class CronTest extends BrowserTestBase {
  use CronRunTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'periodic',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Running cron should update timestamps for periodic tasks.
   */
  public function testInitialCronRun() {
    $tasks = [
      PeriodicEvents::DAY,
      PeriodicEvents::HOUR,
      PeriodicEvents::WEEK,
      PeriodicEvents::MONTH,
    ];

    // Tasks should not exist on initial install.
    foreach ($tasks as $task) {
      $initialTaskState = \Drupal::state()->get('periodic.tasks', []);
      $this->assertArrayNotHasKey($task, $initialTaskState);
    }

    $this->cronRun();
    // Wait to ensure PeriodicManager::destruct() has completed.
    sleep(1);

    $updatedTaskState = \Drupal::state()->get('periodic.tasks', []);
    foreach ($tasks as $task) {
      $this->assertArrayHasKey($task, $updatedTaskState);
      // REQUEST_TIME of cron run should be slightly greater than of this test.
      $this->assertGreaterThanOrEqual(
        \Drupal::time()->getRequestTime(),
        $updatedTaskState[$task]
      );
    }
  }

  /**
   * Running cron should update timestamps for periodic tasks.
   */
  public function testRegularCronRun() {
    $tasks = [
      PeriodicEvents::DAY,
      PeriodicEvents::HOUR,
      PeriodicEvents::WEEK,
      PeriodicEvents::MONTH,
    ];

    // Initialize state so only hourly interval has elapsed.
    $timestamp = \Drupal::time()->getRequestTime() - 4000;
    \Drupal::state()
      ->set('periodic.tasks', array_fill_keys($tasks, $timestamp));

    $this->cronRun();
    // Wait to ensure PeriodicManager::destruct() has completed.
    sleep(1);

    $updatedTaskState = \Drupal::state()->get('periodic.tasks', []);
    foreach ($tasks as $task) {
      // Only the hourly task should have been executed.
      if ($task == PeriodicEvents::HOUR) {
        $this->assertGreaterThanOrEqual(
          \Drupal::time()->getRequestTime(),
          $updatedTaskState[$task]
        );
      }
      else {
        $this->assertEquals($timestamp, $updatedTaskState[$task]);
      }
    }
  }

}
