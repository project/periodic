<?php

namespace Drupal\Tests\periodic\Unit;

use Drupal\Component\EventDispatcher\Event;
use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\periodic\PeriodicEvents;
use Drupal\periodic\PeriodicManager;
use Drupal\Tests\UnitTestCase;
use Prophecy\Argument;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

require __DIR__ . '/../../../periodic.module';

/**
 * Unit test of Periodic cron hook execution.
 *
 * @group periodic
 */
class CronTest extends UnitTestCase {

  /**
   * Mocked Event Dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface|\Prophecy\Prophecy\ObjectProphecy
   */
  private $eventDispatcher;

  /**
   * Mocked Periodic Manager.
   *
   * @var \Drupal\periodic\PeriodicManager|\Prophecy\Prophecy\ObjectProphecy
   */
  private $periodicManager;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    $this->eventDispatcher = $this->prophesize(EventDispatcherInterface::class);
    $this->periodicManager = $this->prophesize(PeriodicManager::class);

    $container = new ContainerBuilder();
    $container->set('event_dispatcher', $this->eventDispatcher->reveal());
    $container->set('periodic.manager', $this->periodicManager->reveal());
    \Drupal::setContainer($container);
  }

  /**
   * Cron hook should dispatch events that have passed their interval.
   */
  public function testCron() {
    $this->periodicManager
      ->execute(Argument::type('string'), Argument::type('int'))
      ->will(function ($args) {
        return $args[0] != PeriodicEvents::HOUR;
      });

    $this->eventDispatcher
      ->dispatch(Argument::type(Event::class), PeriodicEvents::HOUR)
      ->shouldNotBeCalled();
    $this->eventDispatcher
      ->dispatch(Argument::type(Event::class), PeriodicEvents::DAY)
      ->shouldBeCalled();
    $this->eventDispatcher
      ->dispatch(Argument::type(Event::class), PeriodicEvents::WEEK)
      ->shouldBeCalled();
    $this->eventDispatcher
      ->dispatch(Argument::type(Event::class), PeriodicEvents::MONTH)
      ->shouldBeCalled();

    periodic_cron();
  }

}
