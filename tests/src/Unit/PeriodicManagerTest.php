<?php

namespace Drupal\Tests\periodic\Unit;

use Drupal\Component\Datetime\Time;
use Drupal\Core\State\State;
use Drupal\periodic\Event\PeriodicEvent;
use Drupal\periodic\PeriodicManager;
use Drupal\Tests\UnitTestCase;

/**
 * Test the Periodic Manager service.
 *
 * @group periodic
 *
 * @coversDefaultClass \Drupal\periodic\PeriodicManager
 */
class PeriodicManagerTest extends UnitTestCase {

  /**
   * Mock Time service.
   *
   * @var \Drupal\Component\Datetime\Time|\PHPUnit\Framework\MockObject\MockObject
   */
  private $time;

  /**
   * Mock State service.
   *
   * @var \Drupal\Core\State\State|\PHPUnit\Framework\MockObject\MockObject
   */
  private $state;

  /**
   * A Periodic Manager instance for testing.
   *
   * @var \Drupal\periodic\PeriodicManager
   */
  private $manager;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    $this->time = $this->getMockBuilder(Time::class)
      ->disableOriginalConstructor()
      ->getMock();
    $this->time->expects($this->any())
      ->method('getRequestTime')
      ->willReturn(946684800);

    $this->state = $this->getMockBuilder(State::class)
      ->disableOriginalConstructor()
      ->getMock();

    $this->manager = new PeriodicManager($this->state, $this->time);
  }

  /**
   * A new task should be ready to run.
   */
  public function testReadyNewTask() {
    $this->state->expects($this->atLeastOnce())
      ->method('get')
      ->with('periodic.tasks', [])
      ->willReturn([]);
    $this->state->expects($this->never())
      ->method('set');

    $this->assertTrue($this->manager->ready('periodic.test', PeriodicEvent::INTERVAL_HOUR));
    $this->manager->destruct();
  }

  /**
   * A task with an elapsed interval should be ready.
   */
  public function testReadyStaleTask() {
    $this->state->expects($this->atLeastOnce())
      ->method('get')
      ->with('periodic.tasks', [])
      ->willReturn(['periodic.test' => 946681100]);
    $this->state->expects($this->never())
      ->method('set');

    $this->assertTrue($this->manager->ready('periodic.test', PeriodicEvent::INTERVAL_HOUR));
    $this->manager->destruct();
  }

  /**
   * A task should not be ready within its interval.
   */
  public function testReadyFreshTask() {
    $this->state->expects($this->atLeastOnce())
      ->method('get')
      ->with('periodic.tasks', [])
      ->willReturn(['periodic.test' => 946681500]);
    $this->state->expects($this->never())
      ->method('set');

    $this->assertFalse($this->manager->ready('periodic.test', PeriodicEvent::INTERVAL_HOUR));
    $this->manager->destruct();
  }

  /**
   * A new task should run immediately.
   */
  public function testExecuteNewTask() {
    $this->state->expects($this->atLeastOnce())
      ->method('get')
      ->with('periodic.tasks', [])
      ->willReturn([]);
    $this->state->expects($this->once())
      ->method('set')
      ->with('periodic.tasks', ['periodic.test' => 946684800]);

    $this->assertTrue($this->manager->execute('periodic.test', PeriodicEvent::INTERVAL_HOUR));
    $this->manager->destruct();
  }

  /**
   * If first execution is disabled, timestamp should still be updated.
   */
  public function testExecuteDelayedNewTask() {
    $this->state->expects($this->atLeastOnce())
      ->method('get')
      ->with('periodic.tasks', [])
      ->willReturn([]);
    $this->state->expects($this->once())
      ->method('set')
      ->with('periodic.tasks', ['periodic.test' => 946684800]);

    $this->assertFalse($this->manager->execute('periodic.test', PeriodicEvent::INTERVAL_HOUR, FALSE));
    $this->manager->destruct();
  }

  /**
   * A task with an elapsed interval should execute.
   */
  public function testExecuteStaleTask() {
    $this->state->expects($this->atLeastOnce())
      ->method('get')
      ->with('periodic.tasks', [])
      ->willReturn(['periodic.test' => 946681100]);
    $this->state->expects($this->once())
      ->method('set')
      ->with('periodic.tasks', ['periodic.test' => 946684800]);

    $this->assertTrue($this->manager->execute('periodic.test', PeriodicEvent::INTERVAL_HOUR));
    $this->manager->destruct();
  }

  /**
   * Skipping first execution shouldn't affect existing tasks.
   */
  public function testExecuteDelayedStaleTask() {
    $this->state->expects($this->atLeastOnce())
      ->method('get')
      ->with('periodic.tasks', [])
      ->willReturn(['periodic.test' => 946681100]);
    $this->state->expects($this->once())
      ->method('set')
      ->with('periodic.tasks', ['periodic.test' => 946684800]);

    $this->assertTrue($this->manager->execute('periodic.test', PeriodicEvent::INTERVAL_HOUR, FALSE));
    $this->manager->destruct();
  }

  /**
   * A task should not re-execute within its interval.
   */
  public function testExecuteFreshTask() {
    $this->state->expects($this->atLeastOnce())
      ->method('get')
      ->with('periodic.tasks', [])
      ->willReturn(['periodic.test' => 946681500]);
    $this->state->expects($this->never())
      ->method('set');

    $this->assertFalse($this->manager->execute('periodic.test', PeriodicEvent::INTERVAL_HOUR));
    $this->manager->destruct();
  }

  /**
   * Skipping first execution shouldn't affect existing tasks.
   */
  public function testExecuteDelayedFreshTasks() {
    $this->state->expects($this->atLeastOnce())
      ->method('get')
      ->with('periodic.tasks', [])
      ->willReturn(['periodic.test' => 946681500]);
    $this->state->expects($this->never())
      ->method('set');

    $this->assertFalse($this->manager->execute('periodic.test', PeriodicEvent::INTERVAL_HOUR, FALSE));
    $this->manager->destruct();
  }

  /**
   * Multiple task executions should only result in one State::set()
   */
  public function testExecuteMultipleTasks() {
    $this->state->expects($this->atLeastOnce())
      ->method('get')
      ->with('periodic.tasks', [])
      ->willReturn(['periodic.test1' => 946681100]);
    $this->state->expects($this->once())
      ->method('set');

    $this->assertTrue($this->manager->execute('periodic.test1', PeriodicEvent::INTERVAL_HOUR));
    $this->assertTrue($this->manager->execute('periodic.test2', PeriodicEvent::INTERVAL_HOUR));
    $this->assertTrue($this->manager->execute('periodic.test3', PeriodicEvent::INTERVAL_HOUR));
    $this->manager->destruct();
  }

  /**
   * Resetting a task should set its timestamp to zero.
   */
  public function testResetTask() {
    $this->state->expects($this->once())
      ->method('get')
      ->with('periodic.tasks', [])
      ->willReturn([
        'periodic.test1' => 946681500,
        'periodic.test2' => 946681500,
      ]);
    $this->state->expects($this->once())
      ->method('set')
      ->with('periodic.tasks', [
        'periodic.test1' => 0,
        'periodic.test2' => 946681500,
      ]);

    $this->manager->reset('periodic.test1');
    $this->manager->destruct();
  }

  /**
   * Removing a task should unset its timestamp.
   */
  public function testRemoveTask() {
    $this->state->expects($this->once())
      ->method('get')
      ->with('periodic.tasks', [])
      ->willReturn([
        'periodic.test1' => 946681500,
        'periodic.test2' => 946681500,
      ]);
    $this->state->expects($this->once())
      ->method('set')
      ->with('periodic.tasks', [
        'periodic.test2' => 946681500,
      ]);

    $this->manager->remove('periodic.test1');
    $this->manager->destruct();
  }

  /**
   * If all timers are removed, state should be cleaned up.
   */
  public function testRemoveMultiple() {
    $this->state->expects($this->once())
      ->method('get')
      ->with('periodic.tasks', [])
      ->willReturn([
        'periodic.test1' => 946681500,
        'periodic.test2' => 946681500,
      ]);
    $this->state->expects($this->once())
      ->method('delete')
      ->with('periodic.tasks');

    $this->manager->remove('periodic.test1');
    $this->manager->remove('periodic.test2');
    $this->manager->destruct();
  }

  /**
   * If all timers are removed, state should be cleaned up.
   */
  public function testRemoveAll() {
    $this->state->expects($this->once())
      ->method('get')
      ->with('periodic.tasks', [])
      ->willReturn([
        'periodic.test1' => 946681500,
        'periodic.test2' => 946681500,
      ]);
    $this->state->expects($this->once())
      ->method('delete')
      ->with('periodic.tasks');

    $this->manager->removeAll();
    $this->manager->destruct();
  }

}
